package pageObjects;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class CarsPage {

	@FindBy(css="#param_subcat a.button")
	public WebElement carMakerSelect;
	
	@FindBy(css="#param_subcat .subcategories")
	public WebElement carMakerSelectOptions;
	
	@FindBy(css="#param_model a.button")
	public WebElement carModelSelect;
	
	@FindBy(css="#param_motor_year a.button-from")
	public WebElement yearFromField;
	
	@FindBy(css="#param_motor_year a.button-to")
	public WebElement yearToField;
	
	@FindBy(css="#param_price a.button-from")
	public WebElement priceFromField;
	
	@FindBy(css="#param_price .filter-item-from input")
	public WebElement priceFromInput;
	
	@FindBy(css="#param_price a.button-to")
	public WebElement priceToField;
	
	@FindBy(css="#param_price .filter-item-to input")
	public WebElement priceToInput;
	
	@FindBy(css="#param_transmission_type a.button")
	public WebElement transmissionSelect;
	
	@FindBy(css="#param_transmission_type .select li input")
	public List<WebElement> transmissionSelectOptions;
	
	@FindBy(css="#param_motor_engine_size a.button-from")
	public WebElement engineSizeFromField;
	
	@FindBy(css="#param_motor_engine_size a.button-to")
	public WebElement engineSizeToField;
	
	@FindBy(css="#param_color a.button")
	public WebElement colorSelect;
	
	@FindBy(css="#param_motor_mileage a.button-from")
	public WebElement motorMileageFromField;
	
	@FindBy(css="#param_motor_mileage a.button-to")
	public WebElement motorMileageToField;
	
	@FindBy(css="#param_motor_mileage .filter-item-to input")
	public WebElement motorMileageToInput;
	
	@FindBy(css="#param_motor_mileage .filter-item-to ul li a")
	public List<WebElement> motorMileageSelectOptions;
	
	@FindBy(css="#param_car_body a.button")
	public WebElement carBodySelect;
	
	@FindBy(css="#param_fuel_type a.button")
	public WebElement fuelTypeSelect;
	
	@FindBy(id="search-submit")
	public WebElement searchButton;

	@FindBy(id="clear-params")
	public WebElement clearParamsLink;
	
	@FindBy(css=".content table .price strong")
	public List<WebElement> carPricesList;
	
	@FindBy(css="#listContainer.loaderActive")
	public WebElement loader;
	
	
//	Constructor
	public CarsPage(WebDriver driver) {
		PageFactory.initElements(
				new AjaxElementLocatorFactory(driver, 10), 
				this);
    }
	
	public boolean isElementDisplayed(WebElement el) {
		try {
			el.isDisplayed();
		} catch (NoSuchElementException e) {
	        System.out.println(">> ERROR >> " + el + " is not displayed on the page");
			return false;
	    }
		return true;
	}
	
	public void checkMakerSelectHasAllNeededOptions(String[] expectedMakerList) {
		for (String maker: expectedMakerList) {
			Assert.assertTrue(carMakerSelectOptions.getText().contains(maker), "This maker '" + maker + "' wasn't found ->");
		}
	}
	
	public void waitForResults(WebDriver driver) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#listContainer.loaderActive")));
		if(loader != null) {
			for(int i=0; i<10; i++) {
				Thread.sleep(1000);
			}
		}
	}
	
	public void checkPricesAreInRange(int priceFrom, int priceTo) {
		for (WebElement price: carPricesList){
			String str = price.getText();
			int result = Integer.parseInt(str.replaceAll("\\D", ""));
			Assert.assertTrue((priceFrom <=result) && (result<= priceTo), "This " + result + " is not in range ->");
		}
	}
}
