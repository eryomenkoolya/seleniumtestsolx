package testCases;

import java.io.IOException;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import helpers.PropertyValues;
import pageObjects.CarsPage;

public class SearchOnOLXTest {

	private WebDriver driver;
	private PropertyValues prop;
	private CarsPage carsPage;
	private WebDriverWait wait;
	
	String baseUrl="http://olx.ua/transport/legkovye-avtomobili/";
	
	String[] expectedMakerList = {"Все", "Acura", "Alfa Romeo", "Audi", "BMW", "Brilliance", "Caterham", 
			"ChangFeng", "Chery", "Chevrolet", "Chrysler", "Citroen", "Dacia", "Daewoo", "Dodge", 
			"Fiat", "Ford", "Geely", "Great Wall", "Honda", "Hyundai", "Intrall", "Jeep", "Jinbei", 
			"JMC", "Kia", "Koenigsegg", "Land Rover", "Lexus", "Mazda", "Mercedes", "Microcar", 
			"Mitsubishi", "Mitsuoka", "Morgan", "Nissan", "Oldsmobile", "Opel", "Pagani", "Panoz", 
			"Peugeot", "Porsche", "Proton", "PUCH", "Renault", "Roewe", "Seat", "Skoda", "Smart", 
			"Ssang Yong", "Subaru", "Suzuki", "Toyota", "Volkswagen", "Volvo", "ВАЗ", "ГАЗ", "ЗАЗ", 
			"ИЖ", "Москвич", "Таврия", "УАЗ", "Другие"};
	
	@BeforeClass
	public void startBrowser() throws IOException{
		driver = new FirefoxDriver();
		
		prop = new PropertyValues();
		int time = Integer.parseInt(prop.getProp("explicitWaits"));
		wait = new WebDriverWait(driver, time);
		
//		Resize the current window to the given dimension
		Dimension d = new Dimension(500,700);
		driver.manage().window().setSize(d);
		
		carsPage = new CarsPage(driver);
	}
	
	@BeforeMethod
	public void getBaseUrl() {
		driver.get(baseUrl);
	}
	
	@Test (priority = 1)
	public void checkAllFiltersAreDisplayed(){
		carsPage.isElementDisplayed(carsPage.carMakerSelect);
		carsPage.isElementDisplayed(carsPage.carModelSelect);
		carsPage.isElementDisplayed(carsPage.yearFromField);
		carsPage.isElementDisplayed(carsPage.yearToField);
		carsPage.isElementDisplayed(carsPage.priceFromField);
		carsPage.isElementDisplayed(carsPage.priceToField);
		carsPage.isElementDisplayed(carsPage.transmissionSelect);
		carsPage.isElementDisplayed(carsPage.engineSizeFromField);
		carsPage.isElementDisplayed(carsPage.engineSizeToField);
		carsPage.isElementDisplayed(carsPage.colorSelect);
		carsPage.isElementDisplayed(carsPage.motorMileageFromField);
		carsPage.isElementDisplayed(carsPage.motorMileageToField);
		carsPage.isElementDisplayed(carsPage.carBodySelect);
		carsPage.isElementDisplayed(carsPage.fuelTypeSelect);
	}
	
	@Test (priority = 2)
	public void checkMakerList() {
		carsPage.carMakerSelect.click();
		carsPage.checkMakerSelectHasAllNeededOptions(expectedMakerList);
	}
	
	@Test (priority = 3)
	public void checkPriceFields() {
		String priceFromValue1 = "10000";
		String priceFromValue2 = "abs";
		carsPage.priceFromField.click();
		carsPage.priceFromInput.sendKeys(priceFromValue1);
		wait.until(ExpectedConditions.visibilityOf(carsPage.priceFromField));
		Assert.assertTrue(carsPage.priceFromField.getText().contains(priceFromValue1));
		
		carsPage.priceFromField.click();
		carsPage.priceFromInput.clear();
		carsPage.priceFromInput.sendKeys(priceFromValue2);
		carsPage.priceFromInput.sendKeys("\n");
		wait.until(ExpectedConditions.visibilityOf(carsPage.priceFromField));
		Assert.assertFalse(carsPage.priceFromField.getText().contains(priceFromValue2));
	}
	
	@Test (priority = 4)
	public void checkMotorMileageFields() {
		String mileageToValue = "70000";
		carsPage.motorMileageToField.click();
		carsPage.motorMileageToInput.sendKeys(mileageToValue);
		carsPage.searchButton.click();
		wait.until(ExpectedConditions.visibilityOf(carsPage.motorMileageToField));
		Assert.assertTrue(carsPage.motorMileageToField.getText().contains(mileageToValue));
		
		driver.get(baseUrl);
		carsPage.motorMileageToField.click();
		carsPage.motorMileageSelectOptions.get(1).click();
	}
	
	@Test (priority = 5)
	public void checkPriceFilterResult() throws InterruptedException {
		String priceFromValue = "10000";
		String priceToValue = "20000";
		carsPage.priceToField.click();
		carsPage.priceToInput.sendKeys(priceToValue);
		carsPage.priceFromField.click();
		carsPage.priceFromInput.sendKeys(priceFromValue);
		carsPage.waitForResults(driver);
		carsPage.checkPricesAreInRange(Integer.parseInt(priceFromValue), Integer.parseInt(priceToValue));
	}
	
	@Test (priority = 6)
	public void checkTransmissionCheckBox() {
		carsPage.transmissionSelect.click();
		Assert.assertEquals(carsPage.transmissionSelectOptions.get(0).getAttribute("checked"), "true");
		carsPage.transmissionSelectOptions.get(1).click();
		Assert.assertEquals(carsPage.transmissionSelectOptions.get(0).getAttribute("checked"), null);
		Assert.assertEquals(carsPage.transmissionSelectOptions.get(1).getAttribute("checked"), "true");		
	}
	
	@AfterClass
	public void closeBrowser(){
		driver.quit();
	}
}	
